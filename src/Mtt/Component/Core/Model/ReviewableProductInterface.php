<?php
/**
 *
 * @packgage acme
 * @author Yuriy Tkachenko <tds.tkachenko@gmail.com>
 */

namespace Mtt\Component\Core\Model;


interface ReviewableProductInterface extends \Sylius\Component\Core\Model\ReviewableProductInterface
{

}